#include "Tile.h"
#include <ctime>
#include <random>
#include <iostream>

Tile::Tile()
{
}

Tile::Tile(tile_type n)
{
	this->type = n;
	if(VOID == n)
	{
		symbol = char(0xdb);
		setWalls(SPACE, SPACE, SPACE, SPACE);
	}
	else if(BLOCK == n)
	{
		symbol = char(0xff);
		setWalls(WALL, WALL, WALL, WALL);
	}
	else if (LINE_H == n)
	{
		symbol = char(0xc4);
		setWalls(PATH, WALL, PATH, WALL);
	}
	else if (LINE_V == n)
	{
		symbol = char(0xb3);
		setWalls(WALL, PATH, WALL, PATH);
	}
	else if (CROSS == n)
	{
		symbol = char(0xc5);
		setWalls(PATH, PATH, PATH, PATH);
	}
	else if (TCROSS_L == n)
	{
		symbol = char(0xb4);
		setWalls(PATH, PATH, WALL, PATH);
	}
	else if (TCROSS_U == n)
	{
		symbol = char(0xc1);
		setWalls(PATH, PATH, PATH, WALL);
	}
	else if (TCROSS_R == n)
	{
		symbol = char(0xc3);
		setWalls(WALL, PATH, PATH, PATH);
	}
	else if (TCROSS_D == n)
	{
		symbol = char(0xc2);
		setWalls(PATH, WALL, PATH, PATH);
	}
	else if (TURN_LU == n)
	{
		symbol = char(0xd9);
		setWalls(PATH, PATH, WALL, WALL);
	}
	else if (TURN_UR == n)
	{
		symbol = char(0xc0);
		setWalls(WALL, PATH, PATH, WALL);
	}
	else if (TURN_RD == n)
	{
		symbol = char(0xda);
		setWalls(WALL, WALL, PATH, PATH);
	}
	else if (TURN_DL == n)
	{
		symbol = char(0xbf);
		setWalls(PATH, WALL, WALL, PATH);
	}
	else if (END_L == n)
	{
		symbol = char(0xff);
		setWalls(PATH, WALL, WALL, WALL);
	}
	else if (END_U == n)
	{
		symbol = char(0xff);
		setWalls(WALL, PATH, WALL, WALL);
	}
	else if (END_R == n)
	{
		symbol = char(0xff);
		setWalls(WALL, WALL, PATH, WALL);
	}
	else if (END_D == n)
	{
		symbol = char(0xff);
		setWalls(WALL, WALL, WALL, PATH);
	}
}


Tile::~Tile()
{
}

std::vector<tile_type> Tile::getLeftCompliant(const tile_type * Tile_set, int set_size) const
{
	std::vector <tile_type> compliant;
	for (int i = 0; i < set_size; i++)
	{
		if (this->walls.left == Tile(Tile_set[i]).walls.right)
			compliant.push_back(Tile_set[i]);
	}
	return compliant;
}

std::vector<tile_type> Tile::getUpCompliant(const tile_type * Tile_set, int set_size) const
{
	std::vector <tile_type> compliant;
	for (int i = 0; i < set_size; i++)
	{
		if (this->walls.up == Tile(Tile_set[i]).walls.down)
			compliant.push_back(Tile_set[i]);
	}
	return compliant;
}

std::vector<tile_type> Tile::getRightCompliant(const tile_type * Tile_set, int set_size) const
{
	std::vector <tile_type> compliant;
	for (int i = 0; i < set_size; i++)
	{
		if (this->walls.right == Tile(Tile_set[i]).walls.left)
			compliant.push_back(Tile_set[i]);
	}
	return compliant;
}

std::vector<tile_type> Tile::getDownCompliant(const tile_type * Tile_set, int set_size) const
{
	std::vector <tile_type> compliant;
	for (int i = 0; i < set_size; i++)
	{
		if (this->walls.down == Tile(Tile_set[i]).walls.up)
			compliant.push_back(Tile_set[i]);
	}
	return compliant;
}

bool Tile::isCompliantLeft(const Tile & l) const
{
	if (this->walls.left == l.walls.right || l.walls.right == SPACE)
	{
		return true;
	}
	else
		return false;
}

bool Tile::isCompliantUp(const Tile & u) const
{
	if (this->walls.up == u.walls.down || u.walls.down == SPACE)
	{
		return true;
	}
	else
		return false;
}

bool Tile::isCompliantRight(const Tile & r) const
{
	if (this->walls.right == r.walls.left || r.walls.left == SPACE)
	{
		return true;
	}
	else
		return false;
}

bool Tile::isCompliantDown(const Tile & d) const
{
	if (this->walls.down == d.walls.up || d.walls.up == SPACE)
	{
		return true;
	}
	else
		return false;
}

void Tile::printTile() const
{
	std::cout << this->symbol;
}

block Tile::getWalls() const
{
	return this->walls;
}

void Tile::setWalls(wall_type l, wall_type u, wall_type r, wall_type d)
{
	walls.left = l;
	walls.up = u;
	walls.right = r;
	walls.down = d;
}
