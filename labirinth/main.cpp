#include <string>
#include <random>
#include <ctime>
#include <iostream>
#include <vector>
#include "tile.h"
#include "map.h"

int main()
{
	const int N = 70;
	const int M = 100;
	srand(time(NULL));

	Map the_map(N, M);
	the_map.init();
	the_map.generate();
	the_map.show();
	std::cin.get();
	return 0;
}
