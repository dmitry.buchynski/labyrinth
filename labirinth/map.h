#ifndef MAP_H
#define MAP_H
#include <vector>
#include <string>
#include "tile.h"

struct coords {
	int x;
	int y;
};

const int N_of_tiles_ONE_PATH = 4;
const tile_type Tiles_set_ONE_PATH[N_of_tiles_ONE_PATH]{
	END_L,
	END_U,
	END_R,
	END_D
};

const int N_of_tiles_TWO_PATHS = 6;
const tile_type Tiles_set_TWO_PATHS[N_of_tiles_TWO_PATHS]{
	LINE_H,
	LINE_V,
	TURN_LU,
	TURN_UR,
	TURN_RD,
	TURN_DL,
};

const int N_of_tiles_THREE_PATHS = 4;
const tile_type Tiles_set_THREE_PATHS[N_of_tiles_THREE_PATHS]{
	TCROSS_L,
	TCROSS_U,
	TCROSS_R,
	TCROSS_D
};

const int N_of_tiles_FOUR_PATHS = 1;
const tile_type Tiles_set_FOUR_PATHS[N_of_tiles_FOUR_PATHS]{
	CROSS
};

class Map {
private:
	struct {
		int hor;
		int ver;
	} dims;
	int free_tiles;
	std::vector<coords> boarders;
	tile_type **layout;
	coords cur_pos;
public:
	Map(int h = 3, int v = 3);
	~Map();
	void init();
	void generate();
	bool isBoarder(const coords & pos);
	void moveTo(coords next);
	void updateBoarders(const coords & pos);
	tile_type replaceTile(const coords & pos);
	coords chooseTile(std::vector<coords> set);
	std::vector<coords>findPath() const;
	void show() const;
};

Tile generateTile(const Tile & left, const Tile & up, const Tile & right, const Tile & down);

#endif // !MAP_H
