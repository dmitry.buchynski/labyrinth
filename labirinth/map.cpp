#include "map.h"
#include <iostream>

Map::Map(int h, int v)
{
	cur_pos = { 0, 0 };
	free_tiles = (h - 2)*(v - 2);
	dims = { h,v }; 
	layout = new tile_type *[v]; 
	for (int i = 0; i < v; i++) 
		layout[i] = new tile_type[h];
}

Map::~Map()
{
	for (int i = 0; i < dims.ver; i++) 
		delete[] layout[i]; 
	delete[] layout;
}

void Map::init()
{
	cur_pos = { 1, 1 };
	for (int i = 0; i < dims.ver; i++)
		for (int j = 0; j < dims.hor; j++)
			if (0<i && i<dims.ver - 1 && 0 <j && j<dims.hor - 1) layout[i][j] = VOID;
			else layout[i][j] = BLOCK;
			layout[1][0] = LINE_H;
			layout[dims.ver - 2][dims.hor - 1] = LINE_H;
}

void Map::generate()
{
	moveTo({ 1,1 });
	layout[cur_pos.x][cur_pos.y] = generateTile(Tile(layout[cur_pos.x][cur_pos.y - 1]),
		Tile(layout[cur_pos.x - 1][cur_pos.y]),
		Tile(layout[cur_pos.x][cur_pos.y + 1]),
		Tile(layout[cur_pos.x + 1][cur_pos.y])).getType();
	updateBoarders(cur_pos);
	free_tiles--;
	int frame_cnt=0;
	while (free_tiles)
	{
		std::vector<coords>paths;
		/*
		if (0 == (frame_cnt++) % dims.ver)
		{
			system("cls");
			show();
		}
		*/

		paths = findPath();
		if (!paths.empty())
		{
			moveTo(chooseTile(paths));
			layout[cur_pos.x][cur_pos.y]  = generateTile(Tile(layout[cur_pos.x][cur_pos.y - 1]),
				Tile(layout[cur_pos.x - 1][cur_pos.y]),
				Tile(layout[cur_pos.x][cur_pos.y + 1]),
				Tile(layout[cur_pos.x + 1][cur_pos.y])).getType();
			updateBoarders(cur_pos);
			free_tiles--;
		}
		else
		{
			moveTo(chooseTile(boarders));
			layout[cur_pos.x][cur_pos.y] = replaceTile(cur_pos);
		}
	}
}

coords Map::chooseTile(std::vector<coords> set)
{
	return set.at(rand() % set.size());
}

std::vector<coords> Map::findPath() const
{
	Tile cur_tile(layout[cur_pos.x][cur_pos.y]);
	std::vector<coords> result;
	if (VOID == Tile(layout[cur_pos.x - 1][cur_pos.y]).getType() && PATH == cur_tile.getWalls().up)
		result.push_back({ cur_pos.x - 1, cur_pos.y });
	if (VOID == Tile(layout[cur_pos.x][cur_pos.y + 1]).getType() && PATH == cur_tile.getWalls().right)
		result.push_back({ cur_pos.x, cur_pos.y + 1 });
	if (VOID == Tile(layout[cur_pos.x + 1][cur_pos.y]).getType() && PATH == cur_tile.getWalls().down)
		result.push_back({ cur_pos.x + 1, cur_pos.y });
	if (VOID == Tile(layout[cur_pos.x][cur_pos.y - 1]).getType() && PATH == cur_tile.getWalls().left)
		result.push_back({ cur_pos.x, cur_pos.y - 1});
	return result;
}

void Map::show() const
{
	for (int i = 0; i < dims.ver; i++)
	{
		for (int j = 0; j < dims.hor; j++)
			Tile(layout[i][j]).printTile();
		std::cout << std::endl;
	}
}


tile_type Map::replaceTile(const coords & pos)
{
	std::vector<tile_type>TilesType;
	tile_type cur_tile_name = layout[pos.x][pos.y];
	if (std::find(Tiles_set_ONE_PATH, Tiles_set_ONE_PATH + N_of_tiles_ONE_PATH, cur_tile_name) < Tiles_set_ONE_PATH + N_of_tiles_ONE_PATH)
	{
		TilesType.assign(Tiles_set_TWO_PATHS, Tiles_set_TWO_PATHS + N_of_tiles_TWO_PATHS);
	}
	else if (std::find(Tiles_set_TWO_PATHS, Tiles_set_TWO_PATHS + N_of_tiles_TWO_PATHS, cur_tile_name) < Tiles_set_TWO_PATHS + N_of_tiles_TWO_PATHS)
	{
		TilesType.assign(Tiles_set_THREE_PATHS, Tiles_set_THREE_PATHS + N_of_tiles_THREE_PATHS);
	}
	else if (std::find(Tiles_set_THREE_PATHS, Tiles_set_THREE_PATHS + N_of_tiles_THREE_PATHS, cur_tile_name) < Tiles_set_THREE_PATHS + N_of_tiles_THREE_PATHS)
	{
		TilesType.assign(Tiles_set_FOUR_PATHS, Tiles_set_FOUR_PATHS + N_of_tiles_FOUR_PATHS);
	}
	std::vector<tile_type> result;
	for (tile_type tile_t : TilesType)
	{
		Tile temp(tile_t);
		if (temp.isCompliantLeft(Tile(layout[cur_pos.x][cur_pos.y - 1])) &&
			temp.isCompliantUp(Tile(layout[cur_pos.x-1][cur_pos.y])) &&
			temp.isCompliantRight(Tile(layout[cur_pos.x][cur_pos.y+1])) &&
			temp.isCompliantDown(Tile(layout[cur_pos.x+1][cur_pos.y])))
		{
			result.push_back(tile_t);
		}

	}
	if (result.size()>0)
		return Tile(result.at(rand() % result.size())).getType();
	else return VOID;
}


bool Map::isBoarder(const coords & pos)
{
	bool result = false;
	if (0 == pos.x || 0 == pos.y || dims.ver - 1 == pos.x || dims.hor - 1 == pos.y)
		return false;
	Tile cur_tile(layout[pos.x][pos.y]);
	if (layout[pos.x - 1][pos.y] == VOID ||
		layout[pos.x][pos.y + 1] == VOID ||
		layout[pos.x + 1][pos.y] == VOID ||
		layout[pos.x][pos.y - 1] == VOID)
		result = true;
	return result;
}

void Map::updateBoarders(const coords & pos)
{
	for(int i = 0; i<boarders.size(); )
	{
		if (!isBoarder(boarders.at(i)))
		{
			boarders.erase(boarders.begin() + i);
			continue;
		}
		i++;
	}
	if (isBoarder(pos)) boarders.push_back(pos);

}

void Map::moveTo(coords next)
{
	cur_pos = next;
}


Tile generateTile(const Tile & left, const Tile & up, const Tile & right, const Tile & down)
{
	std::vector<tile_type> result;
	tile_type TilesType1[]{
		END_L,
		END_U,
		END_R,
		END_D,
		LINE_H,
		LINE_V,
		TURN_LU,
		TURN_UR,
		TURN_RD,
		TURN_DL,
	};
	for (tile_type tile_t : TilesType1)
	{
		Tile temp(tile_t);
		if (temp.isCompliantLeft(left) &&
			temp.isCompliantUp(up) &&
			temp.isCompliantRight(right) &&
			temp.isCompliantDown(down))
		{
			result.push_back(tile_t);
		}

	}

	if (result.size()>0)
		return Tile(result.at(rand() % result.size()));
	else return Tile(VOID);
}
