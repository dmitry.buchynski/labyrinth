#pragma once
#include <string>
#include <vector>

const std::string TilesType[]
{
	"h_line",
	"v_line",
	"cross",
	"l_tcross",
	"u_tcross",
	"r_tcross",
	"d_tcross",
	"lu_turn",
	"ur_turn",
	"rd_turn",
	"dl_turn",
	"l_end",
	"u_end",
	"r_end",
	"d_end",
};

enum tile_type_enum
{
	BLOCK = 0,
	END_L,
	END_U,
	END_R,
	END_D,
	LINE_H,
	LINE_V,
	TURN_LU,
	TURN_UR,
	TURN_RD,
	TURN_DL,
	TCROSS_L,
	TCROSS_U,
	TCROSS_R,
	TCROSS_D,
	CROSS,
	VOID
};

enum wall_type{WALL, PATH, SPACE};

struct block
{
	wall_type left;
	wall_type up;
	wall_type right;
	wall_type down;
};

typedef tile_type_enum tile_type;

const int Tiles_num = 17;
const tile_type Tiles_set_ALL[Tiles_num]{
	BLOCK,
	END_L,
	END_U,
	END_R,
	END_D,
	LINE_H,
	LINE_V,
	TURN_LU,
	TURN_UR,
	TURN_RD,
	TURN_DL,
	TCROSS_L,
	TCROSS_U,
	TCROSS_R,
	TCROSS_D,
	CROSS,
	VOID
};

class Tile
{
public:
	Tile();
	Tile(tile_type type);
	~Tile();
	tile_type getType() const { return type; };
	std::vector<tile_type> getLeftCompliant(const tile_type * Tile_set = Tiles_set_ALL, int set_size = Tiles_num) const;
	std::vector<tile_type> getUpCompliant(const tile_type * Tile_set = Tiles_set_ALL, int set_size = Tiles_num) const;
	std::vector<tile_type> getRightCompliant(const tile_type * Tile_set = Tiles_set_ALL, int set_size = Tiles_num) const;
	std::vector<tile_type> getDownCompliant(const tile_type * Tile_set = Tiles_set_ALL, int set_size = Tiles_num) const;
	bool isCompliantLeft(const Tile & l) const;
	bool isCompliantUp(const Tile & u) const;
	bool isCompliantRight(const Tile & r) const;
	bool isCompliantDown(const Tile & d) const;
	void printTile() const;
	block getWalls() const;
private:
	tile_type type;
	char symbol;
	block walls;

	void setWalls(wall_type l, wall_type u, wall_type r, wall_type d);
};
